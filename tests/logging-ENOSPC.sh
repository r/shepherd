# GNU Shepherd --- Test logging behavior upon ENOSPC.
# Copyright © 2024-2025 Ludovic Courtès <ludo@gnu.org>
#
# This file is part of the GNU Shepherd.
#
# The GNU Shepherd is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# The GNU Shepherd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the GNU Shepherd.  If not, see <http://www.gnu.org/licenses/>.

shepherd --version
herd --version

if unshare -r true
then :
else
    echo "Linux unprivileged user namespaces are not supported." >&2
    exit 77
fi

if df --version
then :
else
    echo "GNU df (Coreutils) is missing." >&2
    exit 77
fi

log_directory="$(mktemp -d)"
socket="t-socket-$$"
conf="t-conf-$$"
log="$log_directory/t-log-$$"
pid="t-pid-$$"

herd="herd -s $socket"

trap "rmdir $log_directory; rm -f $socket $conf $pid;
      kill \$main_pid || true" EXIT

cat > "$conf"<<EOF
(register-services
  (list (service
          '(test)
          #:start (make-forkexec-constructor
                   '("$SHELL" "-c" "while true; do df -h $log_directory; sleep 0.1; done"))
          #:stop (make-kill-destructor))))
EOF

rm -f "$pid"

# Run the given command in a mount namespace where $log_directory is on a very
# small tmpfs, such that we can simulate lack of disk space for logging.
function run_short_on_disk_space
{
    unshare -mrf sh -c 'mount -t tmpfs -o size=10K tmpfs "'"$log_directory"'"; '"$*"
}

run_short_on_disk_space df -h "$log_directory"

run_short_on_disk_space \
shepherd -I -s "$socket" -c "$conf" -l "$log" --pid="$pid" &
main_pid=$!

# Wait till it's ready.
until test -f "$pid" ; do sleep 0.3 ; done

$herd status
$herd start test

# Wait until $log_directory is full by checking the output of 'df -h'.  From
# then on, attempts to write to the log file all result in ENOSPC.
until $herd status test | grep "100%"
do
    $herd status test
    sleep 1
done

# Wait a bit more to make sure we hit the point of no return.
sleep 2

# At this point, shepherd must still be responsive; new messages should still
# be visible in the output of 'herd status'.
$herd status test
$herd stop test		   # to avoid filling the output of 'herd status root'
$herd eval root '(display (string-append (make-string 1000 #\a) "\n"))'
$herd status root
$herd status root | grep "aaaaaa"
$herd eval root '(display "alive and kicking\n")'
$herd status root | grep "alive"
$herd start test

$herd status

# Stopping should work fine.
$herd stop root
