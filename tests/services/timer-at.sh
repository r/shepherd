# GNU Shepherd --- Test timers.
# Copyright © 2024 Ludovic Courtès <ludo@gnu.org>
#
# This file is part of the GNU Shepherd.
#
# The GNU Shepherd is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# The GNU Shepherd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the GNU Shepherd.  If not, see <http://www.gnu.org/licenses/>.

shepherd --version
herd --version

socket="t-socket-$$"
conf="t-conf-$$"
log="t-log-$$"
service_log="t-service-log-$$"
pid="t-pid-$$"
witness="t-witness-$$"

herd="herd -s $socket"

trap "cat $log || true; rm -f $socket $conf $log $service_log $witness;
      test -f $pid && kill \`cat $pid\` || true; rm -f $pid" EXIT

cat > "$conf" <<EOF
(use-modules (shepherd service timer))

(register-services (list (timer-service)))
(start-in-the-background '(timer))
EOF

rm -f "$pid"
shepherd -I -s "$socket" -c "$conf" -l "$log" --pid="$pid" &

# Wait till it's ready.
while ! test -f "$pid" ; do sleep 0.3 ; done

shepherd_pid="`cat $pid`"

function soonish
{
    guile -c '(use-modules (srfi srfi-19))
(let* ((now (current-time time-utc))
       (soon (make-time time-utc 0 (+ 5 (time-second now)))))
  (display (date->string (time-utc->date soon) "~H:~M:~S")))'
}

$herd schedule timer at $(soonish) -- \
      sh -c "echo TIMER; echo success > $PWD/$witness"

until test -f "$witness"; do sleep 0.5; done
grep "sh.*TIMER" "$log"

rm -f "$witness"
$herd schedule timer at $(soonish)				\
      --service-name=timer-with-many-options			\
      --log-file="$service_log"					\
      -d "$PWD" -E "MESSAGE=TIMER2" --				\
      sh -c "echo message: \$MESSAGE; pwd > $PWD/$witness"
until test -f "$witness"; do sleep 0.5; done
grep "message: TIMER2" "$service_log"
grep "timer-with-many-options started" "$log"

$herd schedule timer at whatever && false
$herd schedule timer do something && false

$herd schedule timer at 17:14 --user=does-not-exist -- id && false

$herd stop timer
