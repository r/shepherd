# GNU Shepherd --- Test the transient service maker.
# Copyright © 2024-2025 Ludovic Courtès <ludo@gnu.org>
#
# This file is part of the GNU Shepherd.
#
# The GNU Shepherd is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# The GNU Shepherd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the GNU Shepherd.  If not, see <http://www.gnu.org/licenses/>.

shepherd --version
herd --version

socket="t-socket-$$"
conf="t-conf-$$"
log="t-log-$$"
service_log="t-service-log-$$"
pid="t-pid-$$"

herd="herd -s $socket"

trap "cat $log || true; rm -f $socket $conf $log $service_log;
      test -f $pid && kill \`cat $pid\` || true; rm -f $pid" EXIT

cat > "$conf"<<EOF
(use-modules (shepherd service transient))
(register-services (transient-service))
EOF

rm -f "$pid"
shepherd -I -s "$socket" -c "$conf" -l "$log" --pid="$pid" &

# Wait till it's ready.
until test -f "$pid" ; do sleep 0.3 ; done

shepherd_pid="`cat $pid`"

function insist
{
    "$@" || { sleep 0.5; insist "$@"; }
}

$herd status transient

$herd spawn transient pwd
default_service_directory="$($herd eval root '(display (default-service-directory))' | grep ^/)"
insist grep "pwd.* $default_service_directory" "$log"

$herd spawn transient pwd -d /
insist grep "pwd.* /$" "$log"

$herd spawn transient pwd -d "$PWD"
insist grep "pwd.* $PWD" "$log"

rm -f "$service_log"
$herd spawn transient -E FOO=bar --log-file="$service_log" -- env
insist grep "FOO=bar" "$service_log"

$herd spawn transient -N sleeper -- sleep 120
$herd status sleeper
$herd status sleeper | grep running
$herd stop sleeper
$herd status sleeper && false

$herd spawn transient -E "LINE=Where's the newline?" \
      -- "$SHELL" -c 'echo -n "$LINE"'
until $herd status root | grep "Where's the newline"; do sleep 0.3; done
grep "Where's the newline" "$log"

$herd stop root
